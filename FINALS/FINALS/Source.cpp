#include <iostream>
#include <string>
#include "Stack.h"
#include "Queue.h"

using namespace std;

void main()
{
	int input;
	int size;
	cout << "Enter size of array: ";
	cin >> size;
	system("cls");

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true)
	{
		char choice;
		cout << "Choose:\n[a] Push an element\n[b] Pop an element\n[c] Display\n";
		cin >> choice;

		if (choice == 'A' || choice == 'a')
		{
			cout << "Enter Number: ";
			cin >> input;
			stack.push(input);
			queue.push(input);

			cout << "T O P :\n";
			cout << "Stack: " << stack.top() << endl;
			cout << "Queue: " << queue.top() << endl;

			system("pause");
		}
		if (choice == 'B' || choice == 'b')
		{
			stack.pop();
			queue.pop();

			cout << "T O P :\n";
			cout << "Stack: " << stack.top() << endl;
			cout << "Queue: " << queue.top() << endl;
			system("pause");
		}
		if (choice == 'C' || choice == 'c')
		{
			cout << "S T A C K :\n";
			for (int i = 0; i < stack.getSize(); i++)
				cout << stack[i] << " ";
			cout << "\nQ U E U E :\n";
			for (int i = 0; i < queue.getSize(); i++)
				cout << queue[i] << " ";

			for (int i = 0 ; i < size; i++)
			{
				stack.pop();
				queue.pop();
			}
			
			cout << endl;
			system("pause");
		}

		system("cls");
	}
	
	system("pause");
}


