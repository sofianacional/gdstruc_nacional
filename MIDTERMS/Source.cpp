#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;
	system("cls");

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true)
	{
		// D I S P L A Y
		cout << "ARRAY SIZE: " << size << "\nGenerated array: " << endl;
		cout << "Unordered: \t";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: \t";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		char choice;
		cout << "\n\nOPTIONS:\n[a] Remove element from index\n[b] Search\n[c] Expand Array\n";
		cin >> choice;
		
		if (choice == 'a' || choice == 'A')
		{
			cout << "\n\nEnter index to remove: ";
			int removeElement;
			cin >> removeElement;
			unordered.remove(removeElement - 1);
			ordered.remove(removeElement - 1);
		}
		else if (choice == 'b' || choice == 'B')
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input; 
			cout << endl;
			// S E A R C H   L I N E A R
			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			// S E A R C H   B I N A R Y
			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			
			cout << "\nCOMPARISONS:\n";
			cout << "linear - " << unordered.getComparison() << endl;
			cout << "binary - " << ordered.getComparison() << endl;
		}
		else if (choice == 'c' || choice == 'C')
		{
			cout << "\n\nInput size for expansion: ";
			int expansionSize;
			cin >> expansionSize;

			for (int i = 0; i < expansionSize; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			size += expansionSize;
		}
		system("pause");
		system("cls");
	}
}


