#pragma once
#include <assert.h>

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize) // to be able to add an element
			expand();
		
		//your code goes after this line
		int i;
		for (i = mNumElements - 1; i >= 0 && mArray[i] > value; i--)
		{
			mArray[i + 1] = mArray[i];
		}
		mArray[i + 1] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		//your code goes after this line
		comparison = 0;
		int i = 0;
		int j = mNumElements - 1;
		while (i <= j) // search   
		{
			int mid = (i + j) / 2;

			if (mArray[mid] == val) {
				return mid;
			}
			else if (mArray[mid] > val) {
			j = mid - 1;
			comparison += 1;
			}
			else {
			i = mid + 1;
			comparison += 1;
			}
			mid = (i + j) / 2;
		}
		if (i > j)
			return -1;
	}

	virtual int getComparison()
	{
		return comparison;
	}

private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	int comparison;
	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};