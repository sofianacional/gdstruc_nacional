#include <iostream>
#include <conio.h>
#include <string>
using namespace std;

void printGuild(string guildName, int count, string *membersName)
{
	cout << "Guild " << guildName << endl;
	for (int i = 0; i < count; i++) {
		cout << i+1 << " " << membersName[i] << endl;
	}
	_getch();
}

void main()
{
	string guildName;
	int count = 0;
	
	cout << "Enter Guild Name: ";
	cin >> guildName;

	cout << "Enter Number of Members: ";
	cin >> count;

	string *membersName = new string[count];
	for (int i = 0; i < count; i++ ) {
		cout << "Enter Name for Member " << i + 1 << " : ";
		cin >> membersName[i];
	}
	system("cls");

	printGuild(guildName, count, membersName);
	system("cls");

	while (true)
	{
		// OPERATIONS
		char option;
		cout << "[a] Print all members [b] Replace member\n[c] Add member [d] Delete Member\n";
		cin >> option;
		system("cls");

		if (option == 'a') // print
		{
			cout << "Guild " << guildName << endl;
			for (int i = 0; i < count; i++) {
				cout << i + 1 << " " << membersName[i] << endl;
			}
			_getch();
		}
		else if (option == 'b') // replace
		{
			int memberReplace = 0;
			string newMember;
			cout << "Who would you like to rename? (Number) ";
			cin >> memberReplace;
			cout << "Who would be the new member? ";
			cin >> newMember;
			membersName[memberReplace - 1] = newMember;
		}
		else if (option == 'c') // adding
		{
			count++;
			string *tempArray = new string[count];
			for (int i = 0; i < count-1; i++)
			{
				tempArray[i] = membersName[i];
			}
			string input;
			cout << "Input new member name ";
			cin >> input;
			tempArray[count - 1] = input;
			delete[] membersName;
			membersName = new string[count];
			for (int i = 0; i < count; i++)
			{
				membersName[i] = tempArray[i];
			}
			
		}
		else if (option == 'd') // delete
		{
			int input = 0;
			cout << "Input member to be deleted (Number): ";
			cin >> input;
			swap(membersName[input - 1], membersName[count-1]);
			string* tempArray = new string[count];
			for (int i = 0; i < count; i++)
			{
				tempArray[i] = membersName[i];
			}
			delete[] membersName;
			count--;
			membersName = new string[count];
			for (int i = 0; i < count; i++)
			{
				membersName[i] = tempArray[i];
			}
		}
		system("cls");
	}
	
}