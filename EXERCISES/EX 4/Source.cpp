#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"
using namespace std;

void main()
{
	srand(time(NULL));
	int size, removeElement, searchValue;
	
	cout << "Input size: ";
	cin >> size;

	UnorderedArray<int> numbers(size);
	
	// push
	cout << endl;
	for (int i = 0; i < size; i++)
		numbers.push(rand() % 100 + 1);

	// display
	for (int i = 0; i < numbers.getSize(); i++)
		cout << i + 1 << ". " <<numbers[i] << endl;

	system("pause");
	system("cls");

	// remove
	cout << "\nInput element to remove: ";
	cin >> removeElement;

	numbers.remove(removeElement - 1);

	// display
	for (int i = 0; i < numbers.getSize(); i++)
		cout << i + 1 << ". " << numbers[i] << endl;

	system("pause");
	system("cls");

	// search
	cout << "SEARCH: ";
	cin >> searchValue;

	numbers.linearSearch(searchValue);
	if (numbers.linearSearch(searchValue) > 0) {
		cout << searchValue << " is found at NUMBER " << numbers.linearSearch(searchValue) + 1 << endl;
	}
	else {
		cout << "Number not found\n";
	}

	system("pause");
}