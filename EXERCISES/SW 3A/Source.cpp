// November 4, 2019 Recursion: Sum of Digits
#include <iostream>
using namespace std;

void computeSum(int input, int sum, int num)
{
	if (input <= 0)
	{
		cout << "\b\b= " << sum;
		return;
	}

	num = input % 10;
	sum = sum + num;
	input = input / 10;
	cout << num << " + ";

	computeSum(input, sum, num);
}

void main()
{
	int sum = 0, input, num = 0;

	cout << "Input: ";
	cin >> input;

	computeSum(input, sum, num);
	cout << endl;
	system("pause");
}