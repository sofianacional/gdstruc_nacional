// November 4, 2019 Recursion: Fibonacci
#include <iostream>
using namespace std;

void printFibonacci(int input, int current, int previous)
{
	input--;
	if (input < 0)
		return;

	cout << current << " ";
	int nextTerm = 0;

	nextTerm = current + previous;
	current = previous;
	previous = nextTerm;

	printFibonacci(input, current, previous);
}

void main()
{
	int t1 = 0, t2 = 1, input;

	cout << "Input: ";
	cin >> input;
	
	printFibonacci(input, t1, t2);

	cout << endl;
	system("pause");
}