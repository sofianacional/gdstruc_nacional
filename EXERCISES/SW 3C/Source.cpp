// November 4, 2019 Recursive: Prime Number
#include <iostream>
using namespace std;

bool isPrime(int num, int i = 2)
{
	if (num <= 2)
		return (num == 2) ? true : false;
	if (num % i == 0)
		return false;
	if (i * i > num)
		return true;

	return isPrime(num, i + 1);
}

void main()
{
	int input;
	cout << "Input: ";
	cin >> input;

	cout << "=============\n";
	cout << "Is Prime?\n";
	if (isPrime(input))
		cout << "Yes.\n";
	else
		cout << "No.\n";

	system("pause");
}